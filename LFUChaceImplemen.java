// package AOK_LFU;

import java.util.HashMap;
import java.util.Map;
// import java.util.PriorityQueue;

class Link {
    int value, frequency;

    public Link (int value, int frequency) {
        this.value = value;
        this.frequency = frequency;
    }
}

class LFU {
    int cacheSize;
    Map<Integer, Link> cache;

    public LFU (int chaceSize) {
        this.cacheSize = chaceSize;
        this.cache = new HashMap<Integer, Link>();
    }

    public void increment (int value) {
        if (cache.containsKey(value)) {
            cache.get(value).frequency += 1; 
        }
    }

    public void insert (int value) {
        if (cache.size() == cacheSize) {
            int lfukey = findLFU();
            System.out.println("Cache block " + lfukey + " removed.");
            cache.remove(lfukey);
        }

        Link newLink = new Link(value, 1);
        cache.put(value, newLink);
        System.out.println("Cache block " + value + " inserted.");
    }

    public void refer(int value) {
        if (!cache.containsKey(value)) {
            insert(value);
        } else {
            increment(value);
        }
    }

    public int findLFU () {
        int lfukey = 0;
        int minFrequency = Integer.MAX_VALUE;
        for (Map.Entry<Integer, Link> entry : cache.entrySet()) {
            if (entry.getValue().frequency < minFrequency) {
                minFrequency = entry.getValue().frequency;
                lfukey = entry.getKey();
            }
        }
        return lfukey;
    }
}

public class LFUChaceImplemen {
    public static void main(String[] args) {
        LFU lfuCache = new LFU(4);
        lfuCache.refer(1);
        lfuCache.refer(2);
        lfuCache.refer(1);
        lfuCache.refer(3);
        lfuCache.refer(2);
        lfuCache.refer(4);
        lfuCache.refer(1);
        lfuCache.refer(2);
        lfuCache.refer(2);
        lfuCache.refer(6);
        lfuCache.refer(5);
    }    
}
